//
//  TweetMentionTableViewController.swift
//  Smashtag
//
//  Created by Bulzan Sergiu on 12/04/2017.
//  Copyright © 2017 Stanford University. All rights reserved.
//

import UIKit
import Twitter


class TweetMentionTableViewController: UITableViewController {
    
    
    private var tweetData: TweetData = TweetData()
    
    public var tweet : Twitter.Tweet?{
        didSet{
            tweetData.tweet = tweet
        }
    }//prepare for segue here
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = tableView.rowHeight
        tableView.rowHeight = UITableViewAutomaticDimension
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tweetData.countForType(type: TweetDataType(rawValue: section)!)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(TweetDataType(rawValue: indexPath.section) == TweetDataType.media){
            return 185;//cell height for image
        }
        return 40;
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == TweetDataType.media.rawValue){
            let cell = tableView.dequeueReusableCell(withIdentifier: "photoMentionCell", for: indexPath)
            if let cell = cell as? PhotoMentionTableViewCell{
                cell.imageUrl = tweet?.media[indexPath.row].url
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "tweetMentionCell", for: indexPath)
            let mention = tweetData.tweetMention(for: TweetDataType(rawValue: indexPath.section)!, at: indexPath.row)
            cell.textLabel?.text = mention?.description ?? ""
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return tweetData.dataTypeString(for: TweetDataType(rawValue: section)!);
    }

}
