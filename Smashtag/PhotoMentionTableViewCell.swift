//
//  PhotoMentionTableViewCell.swift
//  Smashtag
//
//  Created by Bulzan Sergiu on 12/04/2017.
//  Copyright © 2017 Stanford University. All rights reserved.
//

import UIKit
import Twitter

class PhotoMentionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var photoView: UIImageView!
    
    
    public var imageUrl : URL?{
        didSet{
            updateUI()
        }
    }
    
    // whenever our public API tweet is set
    // we just update our outlets using this method
    private func updateUI() {
        if let imageUrl = imageUrl{
            photoView.downloadedFrom(url: imageUrl)
        }
    }
    
}

extension UIImageView{
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit){
        contentMode = mode
        URLSession.shared.dataTask(with: url){ (data,response,error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
            else { return }
            DispatchQueue.main.async {
                self.image = image
            }
        }.resume()
    }
    
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit){
        guard let url = URL(string: link) else {
            print("invalid url")
            return
        }
        downloadedFrom(url: url, contentMode: mode)
        
    }
}
