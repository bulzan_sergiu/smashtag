//
//  TweetData.swift
//  Smashtag
//
//  Created by Bulzan Sergiu on 13/04/2017.
//  Copyright © 2017 Stanford University. All rights reserved.
//

import Foundation
import Twitter

enum TweetDataType:Int{
    case media = 0
    case urls = 1
    case hashtags = 2
    case userMentions = 3
}

enum Sections:Int{
    case media = 0
    case urls = 1
    case hashtags = 2
    case userMentions = 3
}


struct TweetData{
    var tweet : Twitter.Tweet?
    
    
    public func tweetMention(for type:TweetDataType, at rowNumber:Int) -> Mention?{
        if let tweet = tweet{
            switch  type {
            case .urls:
                return tweet.urls[rowNumber]
            case .hashtags:
                return tweet.hashtags[rowNumber]
            case .userMentions:
                return tweet.userMentions[rowNumber]
            default:
                return nil
            }
        }
        return nil
    }
    
    public func countForType(type: TweetDataType) -> Int{
        if let tweet = tweet{
            switch  type {
            case .media:
                return tweet.media.count
            case .urls:
                return tweet.urls.count
            case .hashtags:
                return tweet.hashtags.count
            case .userMentions:
                return tweet.userMentions.count
            }
        }
        return 0;
    }
    
    public func dataTypeString(for type: TweetDataType) -> String{
        if(countForType(type: type) > 0){
            switch type{
            case .media:
                return "Photos"
            case .hashtags:
                return "Hashtags"
            case .urls:
                return "Urls"
            case .userMentions:
                return "User Mentions"
            }
        }
        return ""
    }
    
    
}
